﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using TheCrew.Business.Persistance;
using TheCrew.Business.Services.Interfaces;
using TheCrew.Business.Models;

namespace TheCrew.Business.Services
{
    public class GameService : IGameService
    {

        // Will move away from a singleton eventually, HA'd or DB etc.
        Dictionary<string, Models.Game> games = GameCache.Instance.Games;

        public bool AddPlayer(string id, Models.Player player)
        {
            if (IsPlayerPresentInGame(id, player.Id))
                return false;

            GetById(id).GameInstance.AddPlayer(player.PlayerInstance);

            return true;
        }

        public Models.Game CreateGame(Models.Game game)
        {

            if(game.PlayersCount > 5 || game.PlayersCount < 3)
            {
                throw new ArgumentOutOfRangeException("nbPlayers", "Number of players must be between 3 and 5");
            }


            Game gameInstance = new Game(game.PlayersCount);
            
            game.GameInstance = gameInstance;
            games.Add(game.Id, game);
            return game;
            
        }


        public Models.Game GetById(string id)
        {
            if (!IsGameExists(id))
                throw new ArgumentException($"Game {id} does not exists");

            return GetById(id);
        }

        public bool IsGameExists(string id)
        {
            return games.ContainsKey(id);
            
        }

        public bool IsPlayerPresentInGame(string id, string playerId)
        {
            if (!IsGameExists(id))
                throw new ArgumentException($"Game {id} does not exists");

            return GetById(id).GameInstance.Players.Exists(p => p.Id == playerId);
        }

        public GameStatus PlayCard(string id, string playerId, string card)
        {
            if (!IsPlayerPresentInGame(id, playerId))
                throw new ArgumentException("Unknown player for this game");

            GetById(id).GameInstance.CardPlayed(playerId, card);
            return GetById(id).GameInstance.Status;
        }
    }
}
