﻿using System;
using System.Collections.Generic;
using System.Text;
using TheCrew.Business.Models;

namespace TheCrew.Business.Services.Interfaces
{
    public interface IGameService
    {
        bool IsGameExists(string id);

        bool IsPlayerPresentInGame(string id, string playerId);

        Models.Game GetById(string id);

        Models.Game CreateGame(Models.Game game);

        bool AddPlayer(string gameId, Models.Player player);


       GameStatus PlayCard(string id, string playerId, string card);
    }
}
