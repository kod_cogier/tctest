﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheCrew.Business.Services.Interfaces
{
    public interface IPlayerService
    {
        Models.Player GetById(string id);

        Models.Player Create(Models.Player player);


    }
}
