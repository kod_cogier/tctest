﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheCrew.Business.Models
{
    public class GameStatus
    {
        public List<string> Missions { get; set; }

        public List<string> CompletedMissions { get; set; }

        public string Captain { get; set; }

        public List<string> CurrentRoundCards { get; set; }

        public string WaitingPlayer { get; set; }

        public bool MissionsFinished { get; set; }

        public bool MissionIsSuccess { get; set; }
    }
}
