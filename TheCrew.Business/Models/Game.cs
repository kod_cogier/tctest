﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace TheCrew.Business.Models
{
    [DataContract]
    public class Game
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public bool IsProtected { get; set; }

        [DataMember]
        public string Key { get; set; }

        [DataMember]
        public int PlayersCount { get; set; }

        [DataMember]
        public string Owner { get; set; }

        public Business.Game GameInstance { get; set; }

    }
}
