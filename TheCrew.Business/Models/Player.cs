﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace TheCrew.Business.Models
{
    [DataContract]
    public class Player
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string PublicKey { get; set; }

        
        public TheCrew.Business.Player PlayerInstance { get; set; }
    }
}
