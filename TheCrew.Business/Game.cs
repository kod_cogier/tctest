﻿using Colorful;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using TheCrew.Business.Models;
using Console = Colorful.Console;

namespace TheCrew.Business
{
    public class Game
    {

        public string Id { get; set; }

        StyleSheet styleSheet;

        private static List<string> _deck = new List<string>()
        {
            "1A","2A","3A", "4A", //Atouts fusée
            "1B", "2B", "3B","4B","5B", "6B","7B","8B","9B", //Bleue
            "1V", "2V", "3V","4V","5V", "6V","7V","8V","9V", //Vert
            "1R", "2R", "3R","4R","5R", "6R","7R","8R","9R", //Rose
            "1J", "2J", "3J","4J","5J", "6J","7J","8J","9J" //Jaune
        };
        private static List<string> _missions = new List<string>()
        {

            "1B", "2B", "3B","4B","5B", "6B","7B","8B","9B", //Bleue
            "1V", "2V", "3V","4V","5V", "6V","7V","8V","9V", //Vert
            "1R", "2R", "3R","4R","5R", "6R","7R","8R","9R", //Rose
            "1J", "2J", "3J","4J","5J", "6J","7J","8J","9J" //Jaune
        };

        public List<Player> Players
        {
            get
            {
                return this._players;
            }
        }

        public GameStatus Status 
        {
            get
            {
                return new GameStatus
                {
                    Captain = _captain.Id,
                    CompletedMissions = new List<string>(),
                    CurrentRoundCards = _currentPli,
                    WaitingPlayer = _players[_roundOrderEnumerator.Current].Id,
                    Missions = _currentMissions,
                    MissionsFinished = _currentPli.Contains(_captain.ChoosenMission),
                    MissionIsSuccess = (_roundWinner == _captain)

                };
            }
        }
        private int _nbPlayer;
        private List<Player> _players;
        public Game(int nbPlayers)
        {
            styleSheet = new StyleSheet(Color.White);
            styleSheet.AddStyle("[1-9]R", Color.Red);
                styleSheet.AddStyle("[1-9]B", Color.Blue);
                styleSheet.AddStyle("[1-9]V", Color.Green);
                styleSheet.AddStyle("[1-9]J", Color.Yellow);
                styleSheet.AddStyle("[1-4]A", Color.DarkOrange);
           
            _nbPlayer = nbPlayers;
            _players = new List<Player>();
        }

        public void AddPlayer(Player player)
        {
            _players.Add(player);
        }
        private List<int> _roundOrder = new List<int>();
        private int _currentRoundMissionsCount = 1;

        List<List<string>> _pliHistory = new List<List<string>>();
        List<string> _currentPli = new List<string>();
        List<string> _currentMissions;
        public void NewMission()
        {
            //init players
            Console.WriteLine("New mission");

            _players.ForEach(p =>
            {
                p.NewMission();
            });

            //choose mission
            _currentMissions = _missions.Shuffle<string>().Take(_currentRoundMissionsCount).ToList();

            var currentDeck = _deck.Shuffle<string>();
            var enumerator = currentDeck.GetEnumerator();

            int currentPlayer = 0;
            while (enumerator.MoveNext())
            {
                _players[currentPlayer].AddCard(enumerator.Current);
                currentPlayer = (currentPlayer + 1) % _players.Count;
            }

            _roundWinner = null;
            _captain = _players.First(p => p.IsCaptain);
            _captain.ChoosenMission = _currentMissions.First();

            Console.WriteLineStyled($"Mission is : {_captain.ChoosenMission}",styleSheet);
            Console.WriteLine($"Captain is {_captain.Name}");

            _players.ForEach(p =>
            {
                Console.WriteLineStyled($"Player {p.Name} deck({p.DeckSize}: {p.DumpDeck()}", styleSheet);
            }); 
           
        }
        private IEnumerator<int> _roundOrderEnumerator;
        private Player _roundWinner;
        private Player _captain;
        public void NewRound(Player newFirstPlayer = null)
        {
            Console.WriteLine($"New Round: {_roundCount++}");
            _players.ForEach(p =>
            {
                p.NewRound();
            });
            _currentPli.Clear();

            _roundOrder.Clear();

            
            if (newFirstPlayer == null)
            {
                FirstPlayer = _captain;
            }
            else
            {
                FirstPlayer = newFirstPlayer;
            }

            var firstPlayerOrder = _players.IndexOf(FirstPlayer);

            for(int i = 0; i < _players.Count; i++)
            {
                _roundOrder.Add(firstPlayerOrder);
                firstPlayerOrder = (firstPlayerOrder + 1) % _players.Count;

            }

            _roundOrderEnumerator = _roundOrder.GetEnumerator();

            Console.WriteLine($"Round order is {String.Join(", ", _roundOrder)}");
        }
        private int _roundCount = -1;
        private bool MissionIsFinished() => (_captain.DeckSize == 0);

        private Player FirstPlayer;

        private List<Tuple<string, int>> _advices = new List<Tuple<string, int>>(); 


        private bool IsCardWin(List<string> pli, string card)
        {
            if (pli.Count == 0) return true;
            var pliColor = pli.First()[1];

            if(card[1] != pliColor)
            {
                if(card[1] == 'A')
                {
                    //Atout le pli en contient-l
                    if (!pli.Any(c => c[1] == 'A')) return true;

                    return string.CompareOrdinal(pli.Where(c => c[1] == 'A').OrderByDescending(c => c[0]).First(), 0, card, 0, 1) > 0; 
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return string.CompareOrdinal(pli.Where(c => c[1] == pliColor).OrderByDescending(c => c[0]).First(), 0, card, 0, 1) > 0;
            }
        
        }

        public bool CardPlayed(string playerId, string card)
        {
            if (!_players.Exists(p => p.Id == playerId))
                return false;

            if (Status.WaitingPlayer != playerId)
                return false;

            if (_players.First(p => p.Id == playerId).IsDeckContains(card))
                return false;

            _players.First(p => p.Id == playerId).OnCardPlayed(card);

            return true;
        }
        public bool Play()
        {
            NewMission();
            while (!MissionIsFinished())
            {
                NewRound(_roundWinner);
                foreach (var pIndex in _roundOrder)
                {
                    var currentPlayer = _players[pIndex];

                    var card = currentPlayer.Play(_currentPli, _advices, _captain);
                    Console.WriteLineStyled($"Player {currentPlayer.FullName} play card: {card} {(card == _captain.ChoosenMission?"<--":"")}", styleSheet);
                    
                    if(IsCardWin(_currentPli, card))
                    {
                        _roundWinner = currentPlayer;
                    }
                    _currentPli.Add(card);
                }
                _players.ForEach(p =>
                {
                    p.InspectPli(_currentPli);
                });
                _pliHistory.Add(_currentPli);

                if (_currentPli.Contains(_captain.ChoosenMission))
                {
                    return (_roundWinner == _captain);
                }
                
            }

            return false;
        }

        
    }
}
