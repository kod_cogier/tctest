﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheCrew.Business.Persistance
{
    public class GameCache
    {
        static GameCache _instance = new GameCache();

        static GameCache()
        {
        }
        GameCache()
        {
        }

        public static GameCache Instance
        {
            get { return _instance; }
        }

        private Dictionary<string, Models.Game> _games = new Dictionary<string, Models.Game>();
        public Dictionary<string, Models.Game> Games
        {
            get { return _games; }
        }
    }
}
