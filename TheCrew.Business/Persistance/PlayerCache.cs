﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheCrew.Business.Persistance
{
    public class PlayerCache
    {
        static PlayerCache _instance = new PlayerCache();

        static PlayerCache()
        {
        }
        PlayerCache()
        {
        }

        public static PlayerCache Instance
        {
            get { return _instance; }
        }

        private Dictionary<string, Models.Player> _players = new Dictionary<string, Models.Player>();
        public Dictionary<string, Models.Player> Players
        {
            get { return _players; }
        }
    }
}
