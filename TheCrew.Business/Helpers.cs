﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace TheCrew.Business
{
    public static class Helpers
    {
        public static int GetNextInt32(RNGCryptoServiceProvider rnd)
        {
            byte[] randomInt = new byte[4];
            rnd.GetBytes(randomInt);
            return Convert.ToInt32(randomInt[0]);
        }

        public static List<T> Shuffle<T>(this List<T> list)
        {
            RNGCryptoServiceProvider rnd = new RNGCryptoServiceProvider();
            return ((T[])list.ToArray().Clone()).OrderBy(x => Helpers.GetNextInt32(rnd)).ToList();
        }
    }
}
