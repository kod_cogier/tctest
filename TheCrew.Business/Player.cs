﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Console = Colorful.Console;

namespace TheCrew.Business
{
    public class Player
    {
        public string Id { get; set; }
        private List<string> _deck = new List<string>();

        private Dictionary<char, List<char>> _alreadyPlayedCardByColor = new Dictionary<char, List<char>>
        {
            { 'A', new List<char>() },
            { 'B', new List<char>() },
            { 'R', new List<char>() },
            { 'J', new List<char>() },
            { 'V', new List<char>() }
        };

        public string Name { get; set; }


        public string FullName
        {
            get
            {
                return $"{Name}{(IsCaptain ? "*" : "")}";
            }
        }
        public int DeckSize {  get { return _deck.Count; } }

        public bool IsCaptain
        {
            get;
            private set;
        }

        private bool NeedToTakeLead = false;
        public void AddCard(string card)
        {
            if (card == "4A") IsCaptain = true;
            _deck.Add(card);
        }

        public void NewMission()
        {
            _deck.Clear();
            NewRound();
        }
        public void NewRound()
        {
           
            IsCaptain = false;
        }

        public bool IsDeckContains(string card){
            return _deck.Contains(card);
        }

        public string ChoosenMission;
        public Tuple<string, int> GetAdvice(List<string> mission)
        {
            return null;
        }
        
        public void OnCardPlayed(string card)
        {
            _deck.Remove(card);
        }
        public string Play(List<string> pli, List<Tuple<string, int>> advices, Player captain)
        {
            var mission = captain.ChoosenMission;
            var missionColor = mission[1];

            if (_deck.Count == 0) return null;

            if (pli.Count == 0)
            {
                //I'm the first player of this round
              

                if (!IsCaptain)
                {
                    if(_deck.Any(c => c == captain.ChoosenMission))
                    {
                        var card = _deck.First(c => c.EndsWith($"{missionColor}"));
                        OnCardPlayed(card);
                        return card;
                    }
                    else 
                    {
                        //I don't have0 any card of the mission color
                        //Let's play random color
                        var card = _deck.OrderBy(c => c).First();
                        OnCardPlayed(card);
                        return card;
                    }
                }
                else
                {
                    //I'm the captain
                    if (_deck.Any(c => c == captain.ChoosenMission))
                    {
                        var card = _deck.First(c => c.EndsWith($"{missionColor}"));
                        OnCardPlayed(card);
                        return card;
                    }
                    else
                    {
                        //I don't have0 any card of the mission color
                        //Let's play random color
                        var card = _deck.OrderBy(c => c).First();
                        OnCardPlayed(card);
                        return card;
                    }
                }
            }
            else
            {
                //I'm not the first player
                var pliColor = pli.First()[1];

                if(IsCaptain)
                {
                    if (pli.Contains(mission))
                    {
                        Random rnd = new Random();
                        var card = _deck.Skip(rnd.Next(0, _deck.Count - 1)).Take(1).First();
                        OnCardPlayed(card);
                        return card;
                    }
                    else if (_deck.Contains(mission))
                    {
                        if (missionColor == pliColor)
                        {
                            if (string.CompareOrdinal(pli.Where(c => c[1] == missionColor).OrderByDescending(c => c[0]).First(), 0 , mission,0, 1)> 0)
                            {
                                var card = _deck.Where(c => c[1] == missionColor).OrderBy(c => c[0]).First();
                                OnCardPlayed(card);
                                return card;
                            }
                            else
                            {
                                OnCardPlayed(mission);
                                return mission;
                            }
                        }
                        else
                        {
                            if(_deck.Any(c => c[1] == pliColor))
                            {
                                //I need to play the right color
                                var card = _deck.Where(c => c[1] == pliColor).OrderBy(c => c[0]).First();
                                OnCardPlayed(card);
                                return card;
                            }
                            else
                            {
                                if (NeedToTakeLead && _deck.Any(c => c[1] == 'A'))
                                {
                                    var card = _deck.Where(c => c[1] == missionColor).OrderByDescending(c => c[0]).First();
                                    OnCardPlayed(card);
                                    return card;   
                                }
                                else
                                {
                                    Random rnd = new Random();
                                    var card = _deck.Skip(rnd.Next(0, _deck.Count - 1)).Take(1).First();
                                    OnCardPlayed(card);
                                    return card;
                                }
                            }
                        }
                    }
                    else
                    {
                        Random rnd = new Random();
                        var card = _deck.Skip(rnd.Next(0, _deck.Count - 1)).Take(1).First();
                        OnCardPlayed(card);
                        return card;
                    }
                }
                else
                {
                  //  if (pli.Contains(mission))
                    {
                        Random rnd = new Random();
                        var card = _deck.Skip(rnd.Next(0, _deck.Count - 1)).Take(1).First();
                        OnCardPlayed(card);
                        return card;
                    }
                }
            }
        }

       
        public void InspectPli(List<string> pli)
        {
            pli.ForEach(card =>
            {

                _alreadyPlayedCardByColor[card[1]].Add(card[0]);
            });
        }
        
        public Player()
        {

        }

        public string DumpDeck()
        {
            return String.Join(", ", _deck);
        }
    }
}
