export class Card {

  public suit: string;
  public rank: string;
  private imgPath: string = "/assets/images/cards/";
  private cardPath: string;
  private cardName: string;

  constructor(rank: string, suit: string) {
    this.rank = rank;
    this.suit = suit;
    this.cardPath = this.imgPath + this.getFullRank() + ".png";
    this.cardName = this.getFullRank();
  }

  public getCardDetails() {
    return this.getFullRank();
  }

  public compare(otherCard: Card) {
    if (this.suit < otherCard.suit) {
      return -1;
    } else if (this.suit > otherCard.suit) {
      return 1;
    } else if (this.getRankInt(this.rank) < this.getRankInt(otherCard.rank)) {
      return -1;
    } else if (this.getRankInt(this.rank) > this.getRankInt(otherCard.rank)) {
      return 1;
    } else {
      return 0;
    }
  }

  private getFullRank() {
    return `${this.rank}${this.suit}`;
  }


  private getFullSuit() {
    switch (this.suit) {
      case 'R':
        return "red";
      case 'V':
        return "green";
      case 'J':
        return "yellow";
      case 'B':
        return "blue";
      case 'A':
        return "rocket";
    }
  }

  private getRankInt(rank: string): number {

    switch (rank) {
      case "J":
        return 11;
      case "Q":
        return 12;
      case "K":
        return 13;
      case "A":
        return 14;
      default:
        return Number(rank);
    }
  }
}
