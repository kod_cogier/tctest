import { Injectable } from '@angular/core';
import { Card } from '../card/card';

@Injectable()
export class GameRoomService {

  constructor() { }

  /**
   * Temporary list of cards.
   */
  public getPlayerCards(): Array<Card> {
    let playerCards = new Array<Card>();
    playerCards.push(new Card("2", "A"));
    playerCards.push(new Card("4", "J"));
    playerCards.push(new Card("8", "V"));
    playerCards.push(new Card("2", "J"));
    playerCards.push(new Card("5", "V"));
    playerCards.push(new Card("5", "J"));
    playerCards.push(new Card("3", "J"));
    playerCards.push(new Card("8", "J"));
    return playerCards.sort((x, y) => x.compare(y));
  }

  public getOtherPlayerCards(): Array<Card> {
    let playerCards = new Array<Card>();
    for (var i = 0; i < 8; i++) {
      playerCards.push(new Card("Z", "Z"));
    }
    
    
    return playerCards.sort((x, y) => x.compare(y));
  }

  public clickedCard(card: Card) {
    console.log("User clicked  :" + card.getCardDetails());
  }

}
