import { Component, OnInit } from '@angular/core';
import { GameRoomService } from './game-room.service';
import { Card } from '../card/card';
import { Player } from '../player/player';

@Component({
  selector: 'app-game-room',
  templateUrl: './game-room.component.html',
  styleUrls: ['./game-room.component.css'],
  providers: [GameRoomService]
})
export class GameRoomComponent implements OnInit {

  private listPlayerCards: Array<Card> = [];
  private player1: Player = new Player("Player1");;
  private player2: Player = new Player("Player2");;
  private player3: Player = new Player("Player3");;
  private me: Player = new Player("Christophe", true);

  constructor(private _gameRoomService: GameRoomService) { }

  ngOnInit() {
    this.me.deck = this._gameRoomService.getPlayerCards();
    this.player1.deck = this._gameRoomService.getOtherPlayerCards();
    this.player2.deck = this._gameRoomService.getOtherPlayerCards();
    this.player3.deck = this._gameRoomService.getOtherPlayerCards();
  }

  clickedCard(card: Card) {
    this._gameRoomService.clickedCard(card);
    this.listPlayerCards.push(card);
  }

}
