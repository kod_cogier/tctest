import { Injectable } from '@angular/core';
import { Card } from '../card/card';
import { Player } from './player';
@Injectable()
export class PlayerService {

  constructor() { }

  /**
   * Temporary list of cards.
   */
  public getPlayerCards(): Array<Card> {
    let playerCards = new Array<Card>();
    playerCards.push(new Card("1", "A"));
    playerCards.push(new Card("3", "B"));
    playerCards.push(new Card("4", "R"));
    playerCards.push(new Card("5", "V"));
    return playerCards.sort((x, y) => x.compare(y));
  }

  public clickedCard(card: Card) {
    console.log("User clicked  :" + card.getCardDetails());
  }

}
