"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Player = /** @class */ (function () {
    function Player(name, me) {
        if (me === void 0) { me = false; }
        this.iscaptain = false;
        this.name = name;
        this.deck = [];
        this.me = me;
    }
    Player.prototype.addCardToDeck = function (card) {
        this.deck.push(card);
        if (card.rank == '4' && card.suit == 'A')
            this.iscaptain = true;
    };
    return Player;
}());
exports.Player = Player;
//# sourceMappingURL=player.js.map