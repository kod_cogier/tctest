import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Card } from '../card/card';
import { Player } from './player';
import { PlayerService } from './player.service'
import { GameRoomService } from '../game-room/game-room.service';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css'],
  providers: [PlayerService]
})
export class PlayerComponent implements OnInit {

  @Input()
  private player: Player;

  @Output()
  private clickedCardEmitter: EventEmitter<Card> = new EventEmitter();

  constructor(private _playerService: PlayerService, private _gameroomService: GameRoomService) { }

  ngOnInit() {
    if (this.player == null) {
      this.player = new Player("Player" + new Date().getTime());
    }
  }

 
  clickedCard(card: Card) {
    this._playerService.clickedCard(card);
    this.player.deck.splice(this.player.deck.indexOf(card), 1);
    this._gameroomService.clickedCard(card);

    this.clickedCardEmitter.emit(card);
  }
  
}
