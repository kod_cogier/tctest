import { Card } from "../card/card";

export class Player {

  public name: string;
  public iscaptain: boolean = false;
  public deck: Card[];
  public me: boolean;

  constructor(name: string, me: boolean = false) {
    this.name = name;
    this.deck = [];
    this.me = me;

  }


  addCardToDeck(card: Card) {
    this.deck.push(card);
    if (card.rank == '4' && card.suit == 'A') this.iscaptain = true;
  }
}
