﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheCrew.Web.Interfaces;
using TheCrew.Web.Models;

namespace TheCrew.Web.HubConfigs
{
    public class GameHub:Hub<IGameClient>
    {
        public Task SendCard(Card card)
        {
            return Clients.All.SendCard(card);
        }

    }
}
