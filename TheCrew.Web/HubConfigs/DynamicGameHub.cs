﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheCrew.Web.Models;

namespace TheCrew.Web.HubConfigs
{
    public class DynamicGameHub : DynamicHub
    {
        public async Task SendCard(Card message)
        {
            await Clients.All.Send(message);
        }

        public async Task DistributeCard(string userId, Card card)
        {
            await Clients.Client(userId).Send(card);
        }
    }
}
