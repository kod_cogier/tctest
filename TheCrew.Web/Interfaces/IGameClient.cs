﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheCrew.Web.Models;

namespace TheCrew.Web.Interfaces
{
    public interface IGameClient
    {
        Task SendCard(Card card); 
    }
}
