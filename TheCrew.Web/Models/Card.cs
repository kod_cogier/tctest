﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TheCrew.Web.Models
{
    public class Card
    {
        public char Color { get; set; }

        public char Value { get; set; }
    }
}
