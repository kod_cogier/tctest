﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TheCrew.Business.Models;
using TheCrew.Business.Services.Interfaces;

namespace TheCrew.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GameController : ControllerBase
    {
        readonly IGameService _gamrService;
        readonly IPlayerService _playerService;

        public GameController(IGameService gameService, IPlayerService playerService)
        {
            _gamrService = gameService;
            _playerService = playerService;
        }

        [HttpPost]
        [Route("{id}/player/{playerId}/card")]
        [ProducesResponseType(typeof(GameStatus), StatusCodes.Status200OK)]
        public IActionResult PlayCard(string id, string playerId, string card)
        {
            try
            {
                var status = _gamrService.PlayCard(id, playerId, card);

                if (status != null)
                    return Ok(status);

                return BadRequest();
            }catch(Exception exp)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, exp);
            }
        }


        [HttpPost]
        [Route("")]
        public ActionResult<Game> CreateGame(Game game)
        {
            try
            {
                var newGame = _gamrService.CreateGame(game);
                if (newGame != null)
                    return newGame;
                return BadRequest();
            }
            catch(Exception exp)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, exp);
            }
            
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<Game> Get(string id)
        {
            return _gamrService.GetById(id);
        }

        [HttpPost]
        [Route("{id}/players")]
        public ActionResult<bool> AddPlayer(string id,string playerId)
        {
            try
            {
                var player = _playerService.GetById(playerId);

                return _gamrService.AddPlayer(id, player);


            }catch(Exception exp)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, exp);
            }
            
        }
    }
}
