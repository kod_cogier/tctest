﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Shammill.LobbyManager.Hubs;
using Shammill.LobbyManager.Hubs.Notifiers;
using Shammill.LobbyManager.Models;
using Shammill.LobbyManager.Models.Requests;
using Shammill.LobbyManager.Services.Interfaces;
using Shammill.LobbyManager.Configuration;
using System.Net;
using Microsoft.AspNetCore.Http;

namespace TheCrew.Web.Controllers
{
    [Route("api/[controller]")]
    public class LobbiesController : Controller
    {
        ILobbyService lobbyService;
        IClientNotifier clientNotifier;
        
        public LobbiesController(ILobbyService lobbyService, IClientNotifier clientNotifier)
        {
            this.lobbyService = lobbyService;
            this.clientNotifier = clientNotifier;
        }

#region CRUD
        // GET api/lobbies/{guid}
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(Lobby), StatusCodes.Status200OK)]
        public Lobby Get(Guid lobbyId)
        {
            return lobbyService.GetLobby(lobbyId);
        }

        // GET api/lobbies
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Lobby>), StatusCodes.Status200OK)]
        public IEnumerable<Lobby> Get(LobbyFilter lobbyfilter)
        {
            return lobbyService.GetLobbies(lobbyfilter);
        }


        // POST api/lobbies
        [HttpPost]
        [ProducesResponseType(typeof(Lobby), StatusCodes.Status200OK)]
        public Lobby Post([FromBody]Lobby lobby)
        {
            if (lobby != null)
                return lobbyService.CreateLobby(lobby);

            else return null;
        }

        // PUT api/lobbies/{guid}
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(Lobby), StatusCodes.Status200OK)]
        public Lobby Put(Guid id, [FromBody]Lobby lobby)
        {
            lobbyService.UpdateLobby(lobby);

            clientNotifier.LobbyUpdatedNotifyGroup(lobby.Id.ToString(), new HubMessage { data = lobby });

            return lobby;
        }

        // DELETE api/lobbies/{guid}
        [HttpDelete("{lobbyId}")]
        [ProducesResponseType(typeof(Guid), StatusCodes.Status200OK)]
        public Guid Delete(Guid lobbyId)
        {
            var success = lobbyService.DeleteLobby(lobbyId);

            if (success)
                    clientNotifier.LobbyDeletedNotifyGroup(lobbyId.ToString(), new HubMessage { content = lobbyId.ToString() });
            else
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return lobbyId;
        }
#endregion
    }
}
