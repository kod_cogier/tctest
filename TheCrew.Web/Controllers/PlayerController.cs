﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Shammill.LobbyManager.Configuration;
using Shammill.LobbyManager.Hubs;
using Shammill.LobbyManager.Hubs.Notifiers;
using Shammill.LobbyManager.Models.Requests;
using TheCrew.Business.Services.Interfaces; 
namespace TheCrew.Web.Controllers
{
    [Route("api/[controller]")]
    public class PlayersController : Controller
    {
        IPlayerService _playerService;
        private readonly IClientNotifier clientNotifier;
        public PlayersController(IPlayerService playerService, IClientNotifier clientNotifier)
        {
            this._playerService = playerService;
            this.clientNotifier = clientNotifier;
        }

        // GET api/lobbies/{guid}/players/
        [HttpGet("{id}")]
        public Business.Models.Player Get(string playerId)
        {
            return _playerService.GetById(playerId);
        }

        // POST api/lobbies/{guid}/players/
        [HttpPost]
        [Route("")]
        public ActionResult<Business.Models.Player> Post([FromBody]Business.Models.Player player)
        {

            player.Id = Guid.NewGuid().ToString("N");

            player.PlayerInstance = new Business.Player();


            return _playerService.Create(player);

        }

        

    }
}
