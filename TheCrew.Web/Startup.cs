using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TheCrew.Web.HubConfigs;
using NSwag;
using Shammill.LobbyManager.Services.Interfaces;
using Shammill.LobbyManager.Services;
using Shammill.LobbyManager.Hubs.Notifiers;
using Shammill.LobbyManager.Configuration;
using Shammill.LobbyManager.Hubs;

namespace TheCrew.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });

            //services.AddSignalR();

            services.AddSwaggerDocument();

            services.AddScoped<ILobbyService, LobbyService>();
            services.AddScoped<IPlayerService, PlayerService>();

            if (Config.SignalREnabled)
            {
                services.AddScoped<IClientNotifier, ClientNotifier>();
                services.AddSignalR();
            }
            else
            {
                services.AddScoped<IClientNotifier, DisabledClientNotifier>();
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            if (!env.IsDevelopment())
            {
                app.UseSpaStaticFiles();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3();

            app.UseRouting();


           

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");


                if (Config.SignalREnabled)
                {
                    endpoints.MapHub<SignalRHub>("/signalr");
                }
            });

            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });

            /* app.UseSignalR(options => {
                 options.MapHub<LobbyHub>("/lobby");
             });*/

           
        }
    }
}
