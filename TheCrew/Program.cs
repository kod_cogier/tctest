﻿using System;
using System.Collections.Generic;
using Console = Colorful.Console;
using TheCrew.Business;
namespace TheCrew
{
    class Program
    {
        

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Game game = new Game(5);
            game.AddPlayer(new Player { Name = "Player1" });
            game.AddPlayer(new Player { Name = "Player2" });
            game.AddPlayer(new Player { Name = "Player3" });
            game.AddPlayer(new Player { Name = "Player4" });
            game.AddPlayer(new Player { Name = "Player5" });


            int nbWin = 0;
            int i = 50;

            for(int r = 0; r <i; r++)
            {
                var gameResult = game.Play();

                if (gameResult)
                    nbWin++;
                Console.WriteLine($"Game is finised : {(gameResult ? "Win" : "Fail")}");

            }

            /*
             * i   100
             * y    p
             * 
             * ip = 100y
             * p = 100y / i
             **/

            Console.WriteLine($"End. Win: {nbWin} / Loose:{i - nbWin} : {100 * nbWin / i}");

            
            Console.ReadLine();
        }
    }
}
